#include <stdio.h>
#include <stdlib.h>
#include "stdafx.h" // ... Include library 
#include "gestion_gdt_encoded_block_binary_strut.h"

static const register BOOL    *instructor_inject_load_allocator_structor_signed_constructor(0); // Create signed constructor block
static const register BOOL    *instructor_inject_load_allocator_structor_unsigned_constructor(1); // Create unsigned constructor block
static const register BOOL    *instructor_inject_load_allocator_structor_signed_instructor(0); // Create signed instructor block
static const register BOOL    *instructor_inject_load_allocator_structor_unsigned_instructor(1); // Create unsigned instructor block
static const register BOOL    *instructor_inject_load_allocator_structor_unsigned_save_options(1); // Create unsigned save_options block
static const register BOOL    *instructor_inject_load_allocator_structor_signed_save_option(0); // Create signed save_options block

BOOL naked_encoded_long_blocks1        (asm_Block_Structor()get_allocator++(UINT32 bool_allocator_32_eax_and_ebx_register_Processor); // Create encoded block function 1

BOOL naked_encoded_long_blocks1        (asm_Block_Structor()get_allocator++(UINT16 bool_allocator_32_eax_and_ecx_Register_Processor) // Init eax and ebx registers
{
	bool_allocator_32_eax_and_ebx_Register_Processor = 1; // Init registers eax and ebx
	bool_allocator_32_eax_and_ebx_register_Processor = 0; // Init registers eax and ecx
	
	while((*instructor_inject_load_allocator_structor_unsigned_instructor) == 1) // Repete .............
	{
		
	if((*instructor_inject_load_allocator_structor_unsigned_save_options) ?
	   (*instructor_inject_load_allocator_structor_signed_constructor))
	   return FAILURE;
	
	else if((*instructor_inject_load_allocator_structor_unsigned_constructor) != 
	   (*instructor_inject_load_allocator_structor_signed_instructor))
	   return FAILURE;
	}                         // Test and Compilations .....
}