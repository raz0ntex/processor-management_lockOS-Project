#include <stdio.h>
#include <stdlib.h>
#include "stdafx.h" // ... Include library 

typedef struct encoded_binary_code_and_init_register_eax // Init register eax ...
{
	volatile register BOOL    *eax_binary_value_evolution;
	volatile register BOOL    *eax_value_init;
	volatile register BOOL    *eax_num_break_point;
}encoded_binariy_code_and_init_register_eax;

typedef struct encoded_binary_code_and_init_register_ebx // Init register ebx ...
{
	volatile register BOOL    *ebx_binary_value_evolution;
	volatile register BOOL    *ebx_value_init;
	volatile register BOOL    *ebx_num_break_point;
}encoded_binariy_code_and_init_register_ebx;

typedef struct encoded_binary_code_and_init_register_ecx // Init register ecx ...
{
	volatile register BOOL    *ecx_binary_value_evolution;
	volatile register BOOL    *ecx_value_init;
	volatile register BOOL    *ecx_num_break_point;
}encoded_binariy_code_and_init_register_ecx;

typedef struct encoded_binary_code_and_save_security_register // Save register ... eax ; ebx ; ecx
{
	volatile register _Bool *save_options_register_eax;
	volatile register _Bool *save_options_register_ebx;
	volatile register _Bool *save_options_register_ecx;
	
	volatile register _Bool *break_break_point_register_eax;
	volatile register _Bool *break_break_point_register_ebx;
	volatile register _Bool *break_break_point_register_ecx;
	
	volatile register _Bool *break_binary_register_value_evolution_register_eax;
	volatile register _Bool *break_binary_register_value_evolution_register_ebx;
	volatile register _Bool *break_binary_register_value_evolution_register_ecx;
}encoded_binary_code_and_save_security_register;